from Converter.config import data_list

class Document(object):

    fields = dict()

    def read_changes(self, file):
        with open(file, 'r', encoding='utf-8-sig') as txt:
            lines = txt.readlines()
            is_coords = False
            coords=[]
            for line in lines:
                data = line.split('=')
                if data[-1][-1] == u'\n':
                    data[-1] = data[-1][:-1]
                if len(data) == 2 and not is_coords:
                    self.fields[data[0]] = data[1].strip()
                elif data[0] == 'xy':
                    is_coords = True
                elif is_coords:
                    coords.append(data[0].split(';'))
                elif data[0].isspace():
                    is_coords = False

            if 'ЕГРН' in self.fields.keys():
                self.fields['КПТ'] = self.fields['ЕГРН']
            self.fields['coords'] = coords
            self.fields['КПТ номер'] = self.fields['КПТ'].split(' ')[0]
            self.fields['КПТ номер'] = self.fields['КПТ'].split(' ')[0]
            self.fields['КПТ дата'] = self.fields['КПТ'].split(' ')[1]
            self.fields['Область'] = self.fields['Адрес'].split(', ')[0]
            self.fields['Район'] = self.fields['Адрес'].split(', ')[1]
            self.fields['Населенный пункт'] = self.fields['Адрес'].split(', ')[2]
            self.fields['Улица'] = self.fields['Адрес'].split(', ')[3]







