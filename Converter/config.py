import os

template = 'GKUOKS'

PROJECT_DIR = os.getcwd()
BASE_DIR = os.path.split(PROJECT_DIR)[0]
OUTPUT_DIR = os.path.join(BASE_DIR, 'result')

type_doc = 'GKUOKS'
template_mask = '!tp-builder'
type_mask = '__tp'
data_list = ['Кадастровый квартал',
             'Кадастровый номер земельного участка',
             'Год завершения строительства',
             'Протяженность',
             'Адрес',
             'Выписка из реестра',
             'КПТ',
             ]