import shutil
import os
import uuid
from Converter.doc_object import Document
from Converter.config import OUTPUT_DIR, template_mask, type_mask, type_doc
from Converter.fill_statement_docx import docx_replace


def copy_files(input_dir, output, template):
    '''
    Копирует шаблоны и входные файлы(кроме файла, подходящего по маске) в папку output
    :param input_dir: папка с входными файлами
    :param output: папка с результатом
    :param template: папка с шаблонами
    :return:
    '''
    result_pathes = copy_template(output, template)
    copy_input(input_dir, result_pathes['dir'])
    return result_pathes


def copy_template(output, template_dir):
    '''
    Копирует шаблоны в папку output
    Возвращает словарь, содержащий пути к скпопированным папке, xml, rtf
    :param output: папка output
    :param template_dir: папка с шаблонами
    :returns словарь, содержащий пути к:
     'dir': скпопированным папке,
     'xml': xml,
     'rtf': rtf
    '''

    template_xml_dir = os.path.join(template_dir, type_doc)
    template_xml = os.path.join(template_xml_dir, 'GKUOKS.xml')
    template_rtf = os.path.join(template_dir, 'декларация.rtf')
    template_dop_dir = os.path.join(template_xml_dir, type_doc)

    copy_xml = shutil.copy(template_xml, output)
    copy_rtf = shutil.copy(template_rtf, output)

    doc_guid = str(uuid.uuid4())
    dop_dir_path = os.path.join(output, type_doc) + '_' +doc_guid
    xml_path = dop_dir_path + '.xml'
    rtf_path = dop_dir_path + '.rtf'

    os.rename(copy_xml, xml_path)
    os.rename(copy_rtf, rtf_path)
    shutil.copytree(template_dop_dir, dop_dir_path)
    return {'dir': dop_dir_path, 'rtf': rtf_path, 'xml':xml_path}


def copy_input(input, output):
    '''
    Копирует входные файлы(кроме файла, подходящего по маске) в папку output
    :param input: папка с входными данными
    :param output: папка с результатом
    :return:
    '''
    for file in os.listdir(input):
        if not file.startswith(type_mask):
            file_path = os.path.join(input, file)
            shutil.copy(file_path, output)


def convert(input, output, template):
    list_dirs = os.listdir(input)
    if len(list_dirs) > 0:
        for directory in list_dirs:
            try:
                list_files = os.listdir(os.path.join(input, directory))
            except NotADirectoryError:
                continue

            if len(list_files) > 0:
                for file in list_files:
                    if file.startswith(type_mask):
                        #Создание необходимых папок и переменных
                        if not os.path.exists(output):
                            os.mkdir(output)
                        input_dir = os.path.join(input, directory)
                        file_path = os.path.join(input_dir, file)
                        output_dir = os.path.join(output, directory)
                        if not os.path.exists(output_dir):
                            os.mkdir(output_dir)
                        template_dir = os.path.join(template, template_mask)
                        copy_files(input_dir, output_dir, template_dir)
                        document = Document()
                        document.read_changes(file_path)
                        docx_template = os.path.join(template_dir, 'декларация.docx')
                        docx_result = os.path.join(output_dir, 'result.docx')
                        docx_replace(docx_template, docx_result, document.fields)



if __name__ == '__main__':
    convert( os.path.normpath(r"D:\code\Converter\tp_xml_builder\входные данные\объекты"), OUTPUT_DIR,
             os.path.normpath(r"D:\code\Converter\tp_xml_builder\входные данные"))